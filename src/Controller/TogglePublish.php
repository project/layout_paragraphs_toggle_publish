<?php

namespace Drupal\layout_paragraphs_toggle_publish\Controller;

use Drupal\Core\Ajax\ReplaceCommand;
use Drupal\Core\Ajax\InvokeCommand;
use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\AjaxHelperTrait;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Link;
use Drupal\Core\Url;
use Drupal\layout_paragraphs\Ajax\LayoutParagraphsEventCommand;
use Drupal\layout_paragraphs\LayoutParagraphsLayoutRefreshTrait;
use Drupal\layout_paragraphs\LayoutParagraphsLayout;
use Drupal\layout_paragraphs\LayoutParagraphsLayoutTempstoreRepository;
use Drupal\paragraphs\Entity\Paragraph;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Toggle the published status of a paragraph.
 */
class TogglePublish extends ControllerBase {

  use LayoutParagraphsLayoutRefreshTrait;
  use AjaxHelperTrait;

  /**
   * The tempstore service.
   *
   * @var \Drupal\layout_paragraphs\LayoutParagraphsLayoutTempstoreRepository
   */
  protected $tempstore;

  /**
   * {@inheritDoc}
   */
  public function __construct(LayoutParagraphsLayoutTempstoreRepository $tempstore) {
    $this->tempstore = $tempstore;
  }

  /**
   * {@inheritDoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('layout_paragraphs.tempstore_repository')
    );
  }

  /**
   * Toggle the publishing status of the paragraph.
   */
  public function toggle(LayoutParagraphsLayout $layout_paragraphs_layout, string $component_uuid) {
    $this->setLayoutParagraphsLayout($layout_paragraphs_layout);
    $component = $this->layoutParagraphsLayout->getComponentByUuid($component_uuid);
    $paragraph = $component->getEntity();

    // Toggle the published status of the paragraph.
    $paragraph->isPublished() ? $paragraph->setUnpublished() : $paragraph->setPublished();

    // Create revision and save paragraph.
    $paragraph->setNewRevision(TRUE);
    $paragraph->save();

    $this->layoutParagraphsLayout->setComponent($paragraph);
    $this->tempstore->set($this->layoutParagraphsLayout);

    if ($this->isAjax()) {
      $response = new AjaxResponse();

      $rendered_item = \Drupal::service('renderer')
        ->render($this->getPublishStatusLink($this->layoutParagraphsLayout->id(), $paragraph));

      $response->addCommand(new ReplaceCommand('[data-uuid="' . $component_uuid . '"] .lpb-controls-publish-toggle', $rendered_item));
      $response->addCommand(new InvokeCommand('[data-uuid="' . $component_uuid . '"]', 'toggleClass', ['paragraph--unpublished']));
      $response->addCommand(new LayoutParagraphsEventCommand($this->layoutParagraphsLayout, $paragraph->uuid(), 'component:update'));

      return $response;
    }
  }

  /**
   * Generate the render array for the control link.
   */
  public static function getPublishStatusLink(string $layout_id, Paragraph $entity) {

    $published = !empty($entity) && $entity->isPublished();

    $publish_status_url = Url::fromRoute('layout_paragraphs_toggle_publish.toggle_publish_item', [
      'layout_paragraphs_layout' => $layout_id,
      'component_uuid' => $entity->uuid(),
    ]);
    $publish_status_link = Link::fromTextAndUrl($published ? t('Unpublish') : t('Publish'), $publish_status_url)
      ->toRenderable();
    $publish_status_link_attributes = [
      '#attributes' => [
        'class' => [
          $published ? 'is-published' : 'not-published',
          'lpb-controls-publish-toggle',
          'use-ajax',
        ],
      ],
      '#attached' => [
        'library' => [
          'layout_paragraphs_toggle_publish/toggle_form',
        ],
      ],
      '#access' => \Drupal::currentUser()
        ->hasPermission('view unpublished paragraphs'),
      '#weight' => 55,
    ];

    return array_merge($publish_status_link, $publish_status_link_attributes);
  }

}
