# Layout Paragraphs Toggle Publish

This is a lightweight module to add a toggle button for Paragraphs using the controls in Layout Paragraphs.

## Installation

Installation of this module is the same method as any other Drupal module.

1. Download composer package or via Drupal.org
2. Enable the module

### Install with composer

`composer require drupal/layout_paragraphs_toggle_publish`

### Dependencies

This module requires [Paragraphs](https://www.drupal.org/project/paragraphs)
and [Layout Paragraphs](https://www.drupal.org/project/layout_paragraphs) in order to work.

## Maintainers

Xavier Mirabelli-Montan - [xaviemirmon](https://www.drupal.org/u/xaviemirmon) (Creator)
